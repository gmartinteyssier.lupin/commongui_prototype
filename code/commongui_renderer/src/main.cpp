// Qt includes
#include <QWindow>
#include <QtCore>
#include <QtGui/QGuiApplication>
#include <QtQmlCore/QtQmlCore>

// QML Modules includes
#include <QtQml/qqmlextensionplugin.h>
Q_IMPORT_QML_PLUGIN(CommonGuiComponentsLibPlugin)

int main(int argc, char *argv[])
{
    const QUrl mainQmlUrl(u"qrc:/commongui_renderer/qml/main.qml"_qs);
    const QUrl qrcRootUrl(u":/"_qs);

    QGuiApplication app(argc, argv);

    // Init QmlEngine
    QQmlApplicationEngine engine;
    engine.addImportPath(qrcRootUrl.path());
    engine.load(mainQmlUrl);

    return app.exec();
}
