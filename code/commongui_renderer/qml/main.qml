import QtQuick.Controls
import QtQuick.Window 2.2
import QtQuick.Layouts

Window {
    id: rootWindow

    property bool isFullScreen: Screen.width === 1920 && Screen.height === 1080

    x: _.windowPosX
    y: _.windowPosY

    width: _.windowWidth
    height: _.windowHeight

    // make it not resizable
    maximumWidth: _.windowWidth
    minimumWidth: _.windowWidth
    maximumHeight: _.windowHeight
    minimumHeight: _.windowHeight

    visible: true
    visibility: rootWindow.isFullScreen ? Window.FullScreen : Window.Windowed

    SwipeView {
        id: swipeView

        anchors.fill:parent

        AnimPage{}
        IconsPage {}
        TextPage{}
    }
    Rectangle {
        anchors.fill: swipeView
        color: "grey"
        z: -1
    }

    // private
    QtObject {
        id: _
        // respect ratio of workstation screen
        readonly property int windowWidth: rootWindow.isFullScreen ? Screen.width : 1920
        readonly property int windowHeight: rootWindow.isFullScreen ? Screen.height : 1080

        readonly property int windowPosY: rootWindow.isFullScreen ? 0 : (Screen.height
                                                                         - windowHeight) / 2
        readonly property int windowPosX: rootWindow.isFullScreen ? 0 : windowPosY
    }
}
