import CommonGuiComponentsLib

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts


BasePage{
    color: "darkGrey"

    RowLayout {
        anchors.centerIn: parent
        spacing: 100

        IconPreparationState {
            id: iconPreparationState
            Layout.preferredWidth: 128
            Layout.preferredHeight: 128
        }

        ColumnLayout {
            Layout.preferredWidth: 250
            Button {
                text: "Set to crown"
                Layout.preferredWidth: 250
                onClicked : {
                    iconPreparationState.state = "crown"
                }
            }
            Button {
                text: "Set to veneer"
                Layout.preferredWidth: 250
                onClicked : {
                    iconPreparationState.state = "veneer"
                }
            }
            Button {
                text: "Set to implant"
                Layout.preferredWidth: 250
                onClicked : {
                    iconPreparationState.state = "implant"
                }
            }
        }
    }


}
