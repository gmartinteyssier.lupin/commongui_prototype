import CommonGuiComponentsLib

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts


BasePage{
    color: "darkGrey"

    RowLayout {
        anchors.centerIn: parent
        spacing: 100

        CameraCalibrationImage {
            id: camCalibrationImage
            Layout.preferredWidth: 620
            Layout.preferredHeight: 620
        }

        ColumnLayout {
            Layout.preferredWidth: 250
            Button {
                text: "Robot stabilization"
                Layout.preferredWidth: 250
                onClicked : {
                    camCalibrationImage.state = "robotStabilization"
                }
            }
            Button {
                text: "Patient tracker camera"
                Layout.preferredWidth: 250
                onClicked : {
                    camCalibrationImage.state = "patientTrackerCamera"
                }
            }

            Button {
                text: "Pointer calibration verso"
                Layout.preferredWidth: 250
                onClicked : {
                    camCalibrationImage.state = "pointerCalibrationVerso"
                }
            }

            Button {
                text: "Pointer calibration recto"
                Layout.preferredWidth: 250
                onClicked : {
                    camCalibrationImage.state = "pointerCalibrationRecto"
                }
            }

            Button {
                text: "Robot tracker calibration"
                Layout.preferredWidth: 250
                onClicked : {
                    camCalibrationImage.state = "robotTrackerCalibration"
                }
            }
        }
    }


}
