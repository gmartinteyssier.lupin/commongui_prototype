import QtQuick

MouseArea {
    id: chapterStep
    width: childrenRect.width
    height: childrenRect.height
    state: "state_selected_Yes_step_robotArm"

    property bool isExpanded: chapterStepDescription.visible

    ChapterStepNumber {
        id: chapterStepNumber
        x: 0
        y: 0
        width: 32
        height: 32
        state: "state_iteration_robotArm_background_fillBlue"
    }

    ChapterStepDescription {
        id: chapterStepDescription
        x: 34
        y: 0
        width: 236
        height: 32
        state: "state_iteration_robotArm"
    }

    states: [
        State {
            name: "state_selected_Yes_step_robotArm"
        },
        State {
            name: "state_selected_No_step_robotArm"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_robotArm_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_No_step_calibrate"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_calibration_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_Yes_step_irrigation"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                width: 332
                state: "state_iteration_irrigation"
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_irrigation_background_fillBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 366
            }
        },
        State {
            name: "state_selected_No_step_irrigation"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_irrigation_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_No_step_prepPatient"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_prepPatient_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_No_step_accuracy"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_accuracy_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_Yes_step_calibrate"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                width: 140
                state: "state_iteration_calibrate"
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_calibration_background_fillBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 174
            }
        },
        State {
            name: "state_selected_Yes_step_prepPatient"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                width: 212
                state: "state_iteration_prepPatient"
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_prepPatient_background_fillBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 246
            }
        },
        State {
            name: "state_selected_Yes_step_regPatient"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                width: 224
                state: "state_iteration_registPatient"
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_registerPatient_background_fillBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 258
            }
        },
        State {
            name: "state_selected_No_step_regPatient"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                visible: false
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_registerPatient_background_borderBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 32
            }
        },
        State {
            name: "state_selected_Yes_step_accuracy"
            extend: "state_selected_Yes_step_robotArm"

            PropertyChanges {
                target: chapterStepDescription
                width: 200
                state: "state_iteration_accuracy"
            }

            PropertyChanges {
                target: chapterStepNumber
                state: "state_iteration_accuracy_background_fillBlue"
            }

            PropertyChanges {
                target: chapterStep
                width: 234
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"a0cb084a-1b83-5fdc-8c8a-3afd397ffad0"}
}
##^##*/

