import QtQuick

Rectangle {
    id: preparationIcon
    width: 64
    height: 64
    color: "transparent"
    state: "state_property_1_ic_facette"

    Image {
        id: property_1_ic_facette_merged_child
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_ic_facette_merged_child_state_property_1_ic_facette.svg"
    }

    Image {
        id: property_1_ic_crown_merged_child
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_ic_crown_merged_child_state_property_1_ic_crown.svg"
    }

    Image {
        id: property_1_ic_implant_merged_child
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_ic_implant_merged_child_state_property_1_ic_implant.svg"
    }
    states: [
        State {
            name: "state_property_1_ic_facette"

            PropertyChanges {
                target: property_1_ic_implant_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_ic_crown_merged_child
                visible: false
            }
        },
        State {
            name: "state_property_1_ic_crown"
            extend: "state_property_1_ic_facette"

            PropertyChanges {
                target: property_1_ic_facette_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_ic_crown_merged_child
                visible: true
            }
        },
        State {
            name: "state_property_1_ic_implant"
            extend: "state_property_1_ic_facette"

            PropertyChanges {
                target: property_1_ic_implant_merged_child
                visible: true
            }

            PropertyChanges {
                target: property_1_ic_facette_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_ic_crown_merged_child
                visible: false
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"fd80e0fb-8f7b-5042-83c3-27728bc464b8"}D{i:1;uuid:"04ce02a8-14ed-5456-a6a6-3973aac4cbfa"}
D{i:2;uuid:"7fd1db2f-539c-53f4-97d7-6137ae5cbed8"}D{i:3;uuid:"753bf460-21ed-5db5-acd1-4d70585ece9f"}
}
##^##*/

