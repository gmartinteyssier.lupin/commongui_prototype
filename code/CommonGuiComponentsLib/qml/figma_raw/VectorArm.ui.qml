import QtQuick

Rectangle {
    id: vectorArm
    width: 588
    height: 616
    color: "transparent"

    Image {
        id: vectorArm_merged_child
        x: 3.019
        y: 2
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/vectorArm_merged_child.svg"
    }
}

/*##^##
Designer {
    D{i:0;uuid:"28145e2a-2952-5db0-830f-586955de619e"}D{i:1;uuid:"f283bf30-dec9-5855-b786-e9508951fbbd"}
}
##^##*/

