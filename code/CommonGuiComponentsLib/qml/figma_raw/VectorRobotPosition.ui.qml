import QtQuick

Rectangle {
    id: vectorRobotPosition
    width: 620
    height: 620
    color: "transparent"
    state: "state_property_1_robotOnTheLeft"

    Image {
        id: property_1_robotOnTheLeft_merged_child
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_robotOnTheLeft_merged_child_state_property_1_robotOnTheLeft.svg"
    }

    Image {
        id: property_1_robotOnTheRight_merged_child
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_robotOnTheRight_merged_child_state_property_1_robotOnTheRight.svg"
    }
    states: [
        State {
            name: "state_property_1_robotOnTheLeft"

            PropertyChanges {
                target: property_1_robotOnTheRight_merged_child
                visible: false
            }
        },
        State {
            name: "state_property_1_robotOnTheRight"
            extend: "state_property_1_robotOnTheLeft"

            PropertyChanges {
                target: vectorRobotPosition
                width: 609.742
            }

            PropertyChanges {
                target: property_1_robotOnTheLeft_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_robotOnTheRight_merged_child
                visible: true
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"8ed64df5-50bf-53ad-9251-0473ee03ae4f"}D{i:1;uuid:"8f9876df-eba2-5cd9-84fc-d95f7240412d"}
D{i:2;uuid:"2ce69194-c4e1-5ed5-b3c8-423ed5284542"}
}
##^##*/

