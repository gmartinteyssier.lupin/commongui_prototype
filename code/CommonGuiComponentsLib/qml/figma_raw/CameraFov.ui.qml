import QtQuick

Rectangle {
    id: cameraFov
    width: 478
    height: 575
    color: "transparent"

    Image {
        id: cameraFov1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        source: "../../assets/cameraFov1.svg"
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        anchors.rightMargin: 0
    }
}

/*##^##
Designer {
    D{i:0;uuid:"7e4a7dae-513a-5726-9f43-8b300664973e"}D{i:1;uuid:"50ad6120-3337-5a85-9086-d6d0d519f5ff"}
}
##^##*/

