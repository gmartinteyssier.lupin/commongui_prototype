import QtQuick

Rectangle {
    id: chapterStepDescription
    width: 236
    height: 32
    color: "#ffffff"
    property alias numberTextText: numberText.text
    state: "state_iteration_robotArm"

    Text {
        id: numberText
        x: 16
        y: 5
        width: 205
        height: 22
        color: "#1b24fc"
        text: qsTr("Prepare robot arm")
        font.pixelSize: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.NoWrap
        font.weight: Font.Normal
        font.family: "IBM Plex Mono"
    }
    states: [
        State {
            name: "state_iteration_robotArm"
        },
        State {
            name: "state_iteration_registPatient"
            extend: "state_iteration_robotArm"

            PropertyChanges {
                target: numberText
                width: 193
                text: qsTr("Register patient")
            }

            PropertyChanges {
                target: chapterStepDescription
                width: 224
            }
        },
        State {
            name: "state_iteration_prepPatient"
            extend: "state_iteration_robotArm"

            PropertyChanges {
                target: numberText
                width: 181
                text: qsTr("Prepare patient")
            }

            PropertyChanges {
                target: chapterStepDescription
                width: 212
            }
        },
        State {
            name: "state_iteration_irrigation"
            extend: "state_iteration_robotArm"

            PropertyChanges {
                target: numberText
                width: 301
                text: qsTr("Prepare irrigation system")
            }

            PropertyChanges {
                target: chapterStepDescription
                width: 332
            }
        },
        State {
            name: "state_iteration_accuracy"
            extend: "state_iteration_robotArm"

            PropertyChanges {
                target: numberText
                width: 169
                text: qsTr("Check accuracy")
            }

            PropertyChanges {
                target: chapterStepDescription
                width: 200
            }
        },
        State {
            name: "state_iteration_calibrate"
            extend: "state_iteration_robotArm"

            PropertyChanges {
                target: numberText
                width: 109
                text: qsTr("Calibrate")
            }

            PropertyChanges {
                target: chapterStepDescription
                width: 140
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"001c2620-5a53-5900-b11e-b53bd8068b46"}D{i:1;uuid:"48fc7248-8aad-57d3-be47-d5f4819ff881"}
}
##^##*/

