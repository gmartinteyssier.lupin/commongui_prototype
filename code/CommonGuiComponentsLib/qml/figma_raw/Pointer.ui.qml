import QtQuick

Rectangle {
    id: pointer
    width: 160
    height: 401
    color: "transparent"
    state: "state_property_1_detected"

    Image {
        id: property_1_detected_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_detected_merged_child_state_property_1_detected_1.svg"
    }

    Image {
        id: property_1_ghost_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_ghost_merged_child_state_property_1_ghost_1.svg"
    }
    states: [
        State {
            name: "state_property_1_detected"

            PropertyChanges {
                target: property_1_ghost_merged_child
                visible: false
            }
        },
        State {
            name: "state_property_1_ghost"
            extend: "state_property_1_detected"

            PropertyChanges {
                target: property_1_detected_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_ghost_merged_child
                visible: true
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"2a178ff2-0651-5dac-bc72-5afb6b83406b"}D{i:1;uuid:"4a8427d1-39be-58e6-877c-0b6ed2ca5d89"}
D{i:2;uuid:"6aedc690-36aa-55e4-ad3b-0c51d170322c"}
}
##^##*/

