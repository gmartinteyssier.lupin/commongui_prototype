import QtQuick

Rectangle {
    id: patientTracker
    width: 100.001
    height: 283.001
    color: "transparent"
    state: "state_property_1_detected"

    Image {
        id: property_1_detected_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_detected_merged_child_state_property_1_detected.svg"
    }

    Image {
        id: property_1_ghost_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/property_1_ghost_merged_child_state_property_1_ghost.svg"
    }
    states: [
        State {
            name: "state_property_1_detected"

            PropertyChanges {
                target: property_1_ghost_merged_child
                visible: false
            }
        },
        State {
            name: "state_property_1_ghost"
            extend: "state_property_1_detected"

            PropertyChanges {
                target: property_1_detected_merged_child
                visible: false
            }

            PropertyChanges {
                target: property_1_ghost_merged_child
                visible: true
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"8669f474-f8e4-5b15-89ea-b4f64f452995"}D{i:1;uuid:"03e884c8-acc6-5e9c-820e-9fdadf6789b1"}
D{i:2;uuid:"a5228244-d6f4-57c9-bbc7-5789c10e1d4a"}
}
##^##*/

