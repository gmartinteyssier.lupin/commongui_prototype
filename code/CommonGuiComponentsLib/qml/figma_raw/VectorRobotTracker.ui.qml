import QtQuick

Rectangle {
    id: vectorRobotTracker
    width: 305
    height: 447
    color: "transparent"

    Image {
        id: vectorRobotTracker_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/vectorRobotTracker_merged_child.svg"
    }
}

/*##^##
Designer {
    D{i:0;uuid:"98d7a1cd-2a74-50f5-9abc-1dd6cd4ef415"}D{i:1;uuid:"54e26c40-9b4b-56c4-a0a7-c00b7032fb80"}
}
##^##*/

