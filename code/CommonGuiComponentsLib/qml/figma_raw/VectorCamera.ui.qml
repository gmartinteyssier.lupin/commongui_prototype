import QtQuick

Rectangle {
    id: vectorCamera
    width: 432
    height: 171
    color: "transparent"

    Image {
        id: vectorCamera_merged_child
        x: 0
        y: 0
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "../../assets/vectorCamera_merged_child.svg"
    }
}

/*##^##
Designer {
    D{i:0;uuid:"2850bd6e-7bb5-521a-9aae-cef587117a1c"}D{i:1;uuid:"6ea9340b-6846-5c70-a792-c790740cd671"}
}
##^##*/

