import QtQuick

Rectangle {
    id: chapterBreadcrumb
    width: 658
    height: 64
    color: "transparent"
    state: "state_step_default"

    ChapterStep {
        id: stepRobotArm
        x: 193
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_robotArm"
    }

    ChapterStep {
        id: stepIrrigation
        x: 241
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_irrigation"
    }

    ChapterStep {
        id: stepCalibrate
        x: 289
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_calibrate"
    }

    ChapterStep {
        id: stepPrepPatient
        x: 337
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_prepPatient"
    }

    ChapterStep {
        id: stepRegPatient
        x: 385
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_regPatient"
    }

    ChapterStep {
        id: stepAccuracy
        x: 433
        y: 16
        width: 32
        height: 32
        state: "state_selected_No_step_accuracy"
    }
    states: [
        State {
            name: "state_step_default"
        },
        State {
            name: "state_step_robotArm"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 456
            }

            PropertyChanges {
                target: stepAccuracy
                x: 552
            }

            PropertyChanges {
                target: stepRegPatient
                x: 504
            }

            PropertyChanges {
                target: stepRobotArm
                x: 74
                width: 270
                state: "state_selected_Yes_step_robotArm"
            }

            PropertyChanges {
                target: stepIrrigation
                x: 360
            }

            PropertyChanges {
                target: stepCalibrate
                x: 408
            }
        },
        State {
            name: "state_accuracy"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 236
            }

            PropertyChanges {
                target: stepAccuracy
                x: 332
                width: 234
                state: "state_selected_Yes_step_accuracy"
            }

            PropertyChanges {
                target: stepRegPatient
                x: 284
            }

            PropertyChanges {
                target: stepRobotArm
                x: 92
            }

            PropertyChanges {
                target: stepIrrigation
                x: 140
            }

            PropertyChanges {
                target: stepCalibrate
                x: 188
            }
        },
        State {
            name: "state_irrigation"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 504
            }

            PropertyChanges {
                target: stepAccuracy
                x: 600
            }

            PropertyChanges {
                target: stepRegPatient
                x: 552
            }

            PropertyChanges {
                target: stepRobotArm
                x: 26
            }

            PropertyChanges {
                target: stepIrrigation
                x: 74
                width: 366
                state: "state_selected_Yes_step_irrigation"
            }

            PropertyChanges {
                target: stepCalibrate
                x: 456
            }
        },
        State {
            name: "state_calibrate"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 408
            }

            PropertyChanges {
                target: stepAccuracy
                x: 504
            }

            PropertyChanges {
                target: stepRegPatient
                x: 456
            }

            PropertyChanges {
                target: stepRobotArm
                x: 122
            }

            PropertyChanges {
                target: stepIrrigation
                x: 170
            }

            PropertyChanges {
                target: stepCalibrate
                x: 218
                width: 174
                state: "state_selected_Yes_step_calibrate"
            }
        },
        State {
            name: "state_prepPatient"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 230
                width: 246
                state: "state_selected_Yes_step_prepPatient"
            }

            PropertyChanges {
                target: stepAccuracy
                x: 540
            }

            PropertyChanges {
                target: stepRegPatient
                x: 492
            }

            PropertyChanges {
                target: stepRobotArm
                x: 86
            }

            PropertyChanges {
                target: stepIrrigation
                x: 134
            }

            PropertyChanges {
                target: stepCalibrate
                x: 182
            }
        },
        State {
            name: "state_regPatient"
            extend: "state_step_default"

            PropertyChanges {
                target: stepPrepPatient
                x: 224
            }

            PropertyChanges {
                target: stepAccuracy
                x: 546
            }

            PropertyChanges {
                target: stepRegPatient
                x: 272
                width: 258
                state: "state_selected_Yes_step_regPatient"
            }

            PropertyChanges {
                target: stepRobotArm
                x: 80
            }

            PropertyChanges {
                target: stepIrrigation
                x: 128
            }

            PropertyChanges {
                target: stepCalibrate
                x: 176
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"3a4aa674-8ef9-5ebc-98ec-dc2456def60d"}D{i:1;uuid:"ad1e5ff8-00ad-5a21-a6b2-0aa9118aadcb"}
D{i:2;uuid:"de535ebe-16fb-5deb-9081-96d5b087e5c3"}D{i:3;uuid:"6aa92f55-9c94-56da-9a38-4c37dd2ba51d"}
D{i:4;uuid:"f9e9cbb5-171b-5d99-aefc-c1e2a79510e7"}D{i:5;uuid:"cc526ddd-0704-5109-9ba7-715e434074d4"}
D{i:6;uuid:"9e6ea79e-91ec-5c9d-978b-8974d9fcb19d"}
}
##^##*/

