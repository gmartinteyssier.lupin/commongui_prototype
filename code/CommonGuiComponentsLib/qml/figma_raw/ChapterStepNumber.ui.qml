import QtQuick

Rectangle {
    id: chapterStepNumber
    width: 32
    height: 32
    color: "transparent"
    border.color: "#1b24fc"
    border.width: 1
    property alias numberTextText: numberText.text
    state: "state_iteration_robotArm_background_borderBlue"

    Text {
        id: numberText
        x: 8
        y: 1.333
        width: 17
        height: 29.333
        color: "#1b24fc"
        text: qsTr("1")
        font.pixelSize: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        font.weight: Font.Normal
        font.family: "IBM Plex Mono"
    }
    states: [
        State {
            name: "state_iteration_robotArm_background_borderBlue"
        },
        State {
            name: "state_iteration_robotArm_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        },
        State {
            name: "state_iteration_registerPatient_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
                text: qsTr("5")
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        },
        State {
            name: "state_iteration_irrigation_background_borderBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                text: qsTr("2")
            }
        },
        State {
            name: "state_iteration_irrigation_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
                text: qsTr("2")
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        },
        State {
            name: "state_iteration_calibration_background_borderBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                text: qsTr("3")
            }
        },
        State {
            name: "state_iteration_prepPatient_background_borderBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                text: qsTr("4")
            }
        },
        State {
            name: "state_iteration_registerPatient_background_borderBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                text: qsTr("5")
            }
        },
        State {
            name: "state_iteration_accuracy_background_borderBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                text: qsTr("6")
            }
        },
        State {
            name: "state_iteration_calibration_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
                text: qsTr("3")
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        },
        State {
            name: "state_iteration_prepPatient_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
                text: qsTr("4")
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        },
        State {
            name: "state_iteration_accuracy_background_fillBlue"
            extend: "state_iteration_robotArm_background_borderBlue"

            PropertyChanges {
                target: numberText
                color: "#ffffff"
                text: qsTr("6")
            }

            PropertyChanges {
                target: chapterStepNumber
                color: "#1b24fc"
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"bf88653a-f835-528f-9ee8-7fbc06558659"}D{i:1;uuid:"7cd16fbd-32ac-5026-99e3-111ac7b5506d"}
}
##^##*/

