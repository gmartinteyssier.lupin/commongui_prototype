import QtQuick

Rectangle {
    id: imageCalibrate
    width: 620
    height: 620
    color: "transparent"
    property alias markers_on_patient_trackerText: markers_on_patient_tracker.text
    property alias elementText: element.text
    property alias element1Text: element1.text
    state: "state_property_1_robotStabilization"

    Rectangle {
        id: frame_64
        x: 68
        y: 16.895
        width: 363.905
        height: 586.209
        color: "transparent"
        Item {
            id: group_5
            x: 0
            y: 0
            width: 363.905
            height: 586.209
            Image {
                id: vector_20
                source: "../../assets/vector_20_state_property_1_robotStabilization.svg"
                anchors.verticalCenterOffset: -205.988
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -7.637
            }

            Image {
                id: vector_19
                source: "../../assets/vector_19_state_property_1_robotStabilization.svg"
                anchors.verticalCenterOffset: 52.001
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: 36.8
            }

            Image {
                id: vector_18
                source: "../../assets/vector_18_state_property_1_robotStabilization.svg"
                anchors.verticalCenterOffset: 2.542
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -77.97
            }

            Image {
                id: vector_23
                source: "../../assets/vector_23_state_property_1_robotStabilization.svg"
                anchors.verticalCenterOffset: -0.818
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -82.373
            }
        }
    }

    VectorCamera {
        id: vectorCamera
        width: 198
        height: 78
        anchors.verticalCenterOffset: -215
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: 159
    }

    Text {
        id: element
        width: 78
        height: 64
        color: "#1b24fc"
        text: qsTr("2.")
        font.pixelSize: 64
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.NoWrap
        font.weight: Font.Normal
        anchors.verticalCenterOffset: -80
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: 139
        font.family: "IBM Plex Mono"
    }

    Image {
        id: ellipse_8
        source: "../../assets/ellipse_8_state_property_1_robotStabilization.svg"
        anchors.verticalCenterOffset: 213.393
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -167.722
        rotation: -96.247
    }

    Image {
        id: ellipse_10
        source: "../../assets/ellipse_10_state_property_1_robotStabilization.svg"
        anchors.verticalCenterOffset: 114.393
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: 121.278
        rotation: -96.247
    }

    Image {
        id: ellipse_11
        source: "../../assets/ellipse_11_state_property_1_robotStabilization.svg"
        anchors.verticalCenterOffset: 270.393
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -25.722
        rotation: -96.247
    }

    Text {
        id: element1
        width: 78
        height: 64
        color: "#1b24fc"
        text: qsTr("1.")
        font.pixelSize: 64
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.NoWrap
        font.weight: Font.Normal
        anchors.verticalCenterOffset: 246
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -231
        font.family: "IBM Plex Mono"
    }

    Image {
        id: ellipse_9
        source: "../../assets/ellipse_9_state_property_1_robotStabilization.svg"
        anchors.verticalCenterOffset: 31.151
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: 229.494
        rotation: -35.559
    }

    Text {
        id: markers_on_patient_tracker
        width: 72
        height: 59
        visible: false
        color: "#1b24fc"
        text: qsTr("Markers
on patient
tracker")
        font.pixelSize: 14
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.weight: Font.Normal
        anchors.verticalCenterOffset: -143.5
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -170
        font.family: "IBM Plex Mono"
    }

    CameraFov {
        id: cameraFov
        x: -6
        y: 45
        width: 478
        height: 575
    }

    Rectangle {
        id: frame_63
        x: -50.377
        y: 131.146
        width: 308.377
        height: 472.838
        color: "transparent"
        Item {
            id: group_9
            x: 57.328
            y: 472.838
            width: 443.857
            height: 253.169
            PatientTracker {
                id: patientTrackerDetected
                width: 113.826
                height: 322.128
                anchors.verticalCenterOffset: -409.521
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -97.485
                state: "state_property_1_detected"
                rotation: -13.458
            }

            PatientTracker {
                id: patientTrackerGhost
                width: 117
                height: 330
                anchors.verticalCenterOffset: -306.57
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -165.38
                state: "state_property_1_ghost"
            }
        }
    }

    Image {
        id: vector_17
        source: "../../assets/vector_17_state_property_1_robotTrackerCalibration.svg"
        anchors.verticalCenterOffset: 213
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -19.5
    }

    VectorArm {
        id: vectorArm
        width: 524
        height: 518
        anchors.verticalCenterOffset: 22
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -40
    }

    Image {
        id: vector_16
        source: "../../assets/vector_16_state_property_1_robotTrackerCalibration.svg"
        anchors.verticalCenterOffset: 212.5
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -21.5
    }

    Image {
        id: vector_15
        source: "../../assets/vector_15_state_property_1_robotTrackerCalibration.svg"
        anchors.verticalCenterOffset: -44.5
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -36.5
    }

    VectorRobotTracker {
        id: vectorRobotTracker
        width: 109
        height: 162
        anchors.verticalCenterOffset: 67
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -217.5
    }

    Rectangle {
        id: frame_62
        x: -58.424
        y: 108.889
        width: 367.871
        height: 544.194
        color: "transparent"
        Item {
            id: group_10
            x: 122.07
            y: 544.194
            width: 497.155
            height: 253.563
            Pointer {
                id: pointerGhost
                width: 160
                height: 401
                anchors.verticalCenterOffset: -365.361
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -178.227
                state: "state_property_1_ghost"
            }

            Pointer {
                id: pointerDetected
                width: 160
                height: 401
                anchors.verticalCenterOffset: -435.361
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -203.227
                state: "state_property_1_detected"
                rotation: -6.793
            }
        }
    }

    Rectangle {
        id: frame_61
        x: -93.881
        y: 132.66
        width: 440.375
        height: 525.038
        color: "transparent"
        Item {
            id: group_11
            x: 289.765
            y: 525.038
            width: 447.599
            height: 307.708
            Pointer {
                id: pointerGhost1
                width: 160
                height: 401
                anchors.verticalCenterOffset: -405.054
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -285.679
                state: "state_property_1_ghost"
            }

            Pointer {
                id: pointerDetected1
                width: 161.823
                height: 401.084
                anchors.verticalCenterOffset: -445.532
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: -331.678
                state: "state_property_1_detected"
                rotation: 12.332
            }
        }
    }
    states: [
        State {
            name: "state_property_1_robotStabilization"

            PropertyChanges {
                target: frame_61
                visible: false
            }

            PropertyChanges {
                target: vector_17
                visible: false
            }

            PropertyChanges {
                target: vectorArm
                visible: false
            }

            PropertyChanges {
                target: vector_16
                visible: false
            }

            PropertyChanges {
                target: markers_on_patient_tracker
                visible: false
            }

            PropertyChanges {
                target: vector_15
                visible: false
            }

            PropertyChanges {
                target: frame_62
                visible: false
            }

            PropertyChanges {
                target: vectorRobotTracker
                visible: false
            }

            PropertyChanges {
                target: cameraFov
                visible: false
            }

            PropertyChanges {
                target: frame_63
                visible: false
            }
        },
        State {
            name: "state_property_1_patientTrackerCamera"
            extend: "state_property_1_robotStabilization"

            PropertyChanges {
                target: ellipse_10
                visible: false
            }

            PropertyChanges {
                target: markers_on_patient_tracker
                visible: true
            }

            PropertyChanges {
                target: element1
                visible: false
            }

            PropertyChanges {
                target: ellipse_9
                visible: false
            }

            PropertyChanges {
                target: element
                visible: false
            }

            PropertyChanges {
                target: ellipse_8
                visible: false
            }

            PropertyChanges {
                target: vectorCamera
                width: 422
                height: 167
                anchors.verticalCenterOffset: -143.5
                anchors.horizontalCenterOffset: 93
            }

            PropertyChanges {
                target: cameraFov
                visible: true
            }

            PropertyChanges {
                target: frame_63
                visible: true
            }

            PropertyChanges {
                target: ellipse_11
                visible: false
            }

            PropertyChanges {
                target: frame_64
                visible: false
            }
        },
        State {
            name: "state_property_1_pointerCalibrationVerso"
            extend: "state_property_1_robotStabilization"

            PropertyChanges {
                target: frame_61
                visible: true
            }

            PropertyChanges {
                target: vector_17
                visible: false
            }

            PropertyChanges {
                target: vectorArm
                visible: false
            }

            PropertyChanges {
                target: vector_16
                visible: false
            }

            PropertyChanges {
                target: ellipse_10
                visible: false
            }

            PropertyChanges {
                target: markers_on_patient_tracker
                visible: false
            }

            PropertyChanges {
                target: vector_15
                visible: false
            }

            PropertyChanges {
                target: element1
                visible: false
            }

            PropertyChanges {
                target: ellipse_9
                visible: false
            }

            PropertyChanges {
                target: frame_62
                visible: false
            }

            PropertyChanges {
                target: element
                visible: false
            }

            PropertyChanges {
                target: ellipse_8
                visible: false
            }

            PropertyChanges {
                target: vectorRobotTracker
                visible: false
            }

            PropertyChanges {
                target: vectorCamera
                width: 422
                height: 167
                anchors.verticalCenterOffset: -143.5
                anchors.horizontalCenterOffset: 93
            }

            PropertyChanges {
                target: cameraFov
                x: 26
                y: 32
                visible: true
            }

            PropertyChanges {
                target: frame_63
                visible: false
            }

            PropertyChanges {
                target: ellipse_11
                visible: false
            }

            PropertyChanges {
                target: frame_64
                visible: false
            }
        },
        State {
            name: "state_property_1_pointerCalibrationRecto"
            extend: "state_property_1_robotStabilization"

            PropertyChanges {
                target: vector_17
                visible: false
            }

            PropertyChanges {
                target: vectorArm
                visible: false
            }

            PropertyChanges {
                target: vector_16
                visible: false
            }

            PropertyChanges {
                target: ellipse_10
                visible: false
            }

            PropertyChanges {
                target: markers_on_patient_tracker
                visible: false
            }

            PropertyChanges {
                target: vector_15
                visible: false
            }

            PropertyChanges {
                target: element1
                visible: false
            }

            PropertyChanges {
                target: ellipse_9
                visible: false
            }

            PropertyChanges {
                target: frame_62
                visible: true
            }

            PropertyChanges {
                target: element
                visible: false
            }

            PropertyChanges {
                target: ellipse_8
                visible: false
            }

            PropertyChanges {
                target: vectorRobotTracker
                visible: false
            }

            PropertyChanges {
                target: vectorCamera
                width: 422
                height: 167
                anchors.verticalCenterOffset: -143.5
                anchors.horizontalCenterOffset: 93
            }

            PropertyChanges {
                target: cameraFov
                x: 26
                y: 26
                visible: true
            }

            PropertyChanges {
                target: frame_63
                visible: false
            }

            PropertyChanges {
                target: ellipse_11
                visible: false
            }

            PropertyChanges {
                target: frame_64
                visible: false
            }
        },
        State {
            name: "state_property_1_robotTrackerCalibration"
            extend: "state_property_1_robotStabilization"

            PropertyChanges {
                target: vector_17
                visible: true
            }

            PropertyChanges {
                target: vectorArm
                visible: true
            }

            PropertyChanges {
                target: vector_16
                visible: true
            }

            PropertyChanges {
                target: ellipse_10
                visible: false
            }

            PropertyChanges {
                target: markers_on_patient_tracker
                visible: false
            }

            PropertyChanges {
                target: vector_15
                visible: true
            }

            PropertyChanges {
                target: element1
                visible: false
            }

            PropertyChanges {
                target: ellipse_9
                visible: false
            }

            PropertyChanges {
                target: element
                visible: false
            }

            PropertyChanges {
                target: vectorRobotTracker
                visible: true
            }

            PropertyChanges {
                target: ellipse_8
                visible: false
            }

            PropertyChanges {
                target: vectorCamera
                anchors.verticalCenterOffset: -175
                anchors.horizontalCenterOffset: 199
            }

            PropertyChanges {
                target: cameraFov
                x: 252
                y: 44
                width: 299
                height: 407
                visible: true
            }

            PropertyChanges {
                target: frame_63
                visible: false
            }

            PropertyChanges {
                target: ellipse_11
                visible: false
            }

            PropertyChanges {
                target: frame_64
                visible: false
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;uuid:"7a1909aa-2b1e-5d14-aa6e-13cc176cf7aa"}D{i:3;uuid:"11c80b77-d56c-54e8-aaca-fd7cb7ea0282"}
D{i:4;uuid:"a23a1a17-47f3-527c-ac18-108642b3f2f1"}D{i:5;uuid:"29372a36-cf18-50d2-b984-c8dcd4a3c32a"}
D{i:6;uuid:"3878bd64-b693-5b25-983c-eae471cdc12a"}D{i:2;uuid:"95b288f2-09ba-5f37-976b-423043dd2f40"}
D{i:1;uuid:"e4effa81-8782-56a7-bb0b-202a2dc43f69"}D{i:7;uuid:"7460565b-79e8-53d0-85ca-8792d112f011"}
D{i:8;uuid:"6e432e05-c71c-5bf7-9642-349fd512873d"}D{i:9;uuid:"8fea2b3c-5ac1-5d24-ad04-3cded061de6c"}
D{i:10;uuid:"3253cd93-b5dc-554e-a351-c86eb99115ce"}D{i:11;uuid:"47b3cb5b-8498-581c-bfe6-95a72c760330"}
D{i:12;uuid:"8ac7c9f4-aa6d-5ef1-9357-71d4be25bb94"}D{i:13;uuid:"01b65d83-1c9c-5149-ac98-f82e940e98d2"}
D{i:14;uuid:"23a666ff-4495-551b-bc78-8bbcc6626c68"}D{i:15;uuid:"babdd63a-7902-57d2-bef7-38f3fd00c7ef"}
D{i:18;uuid:"b0a9eef4-9c31-587f-a4db-96b81d3a6337"}D{i:19;uuid:"44ad7932-8af9-5994-90a9-d62c233be2ef"}
D{i:17;uuid:"615cc511-9e15-551c-972a-633f6a08dd36"}D{i:16;uuid:"7129bb6c-43e7-5164-b4ca-343162020d48"}
D{i:20;uuid:"d62d776d-e4e8-59d3-8d66-a9472fe77fa9"}D{i:21;uuid:"2fdb379b-86ae-5dd2-a977-12bebf11dab4"}
D{i:22;uuid:"d7d3d263-1895-5f3c-bf11-4c3327d2779f"}D{i:23;uuid:"a6ac611b-eb46-5dca-acec-c163a7fe0015"}
D{i:24;uuid:"6e24d337-7e17-543a-a34e-ff58f034bf8f"}D{i:27;uuid:"168e97fb-7440-5669-87a7-5f25c4540fd5"}
D{i:28;uuid:"47ae43e4-b55b-5eef-b3c6-ecee565a94af"}D{i:26;uuid:"b1032c2f-6865-54cc-93a2-1c614d6660da"}
D{i:25;uuid:"44cd20ce-d62f-5b1e-a720-37184b887a02"}D{i:31;uuid:"88387234-65c6-5b35-9e64-444331c5d9ce"}
D{i:32;uuid:"7cff90f3-e6f9-55bc-bd5c-d6082f8239ba"}D{i:30;uuid:"fd42d8fd-3c53-509e-bc88-c549d2c63ce8"}
D{i:29;uuid:"bfaaf398-3fd5-540b-b68c-eed9159e74de"}
}
##^##*/

