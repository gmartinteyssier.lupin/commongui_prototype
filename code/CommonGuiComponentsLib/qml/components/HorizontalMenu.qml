import QtQuick
import QtQuick.Layouts

import "../figma_raw"

Row {
    state: "default"

    spacing: 10

    ChapterStep {
        id: stepRobotArm
        onClicked: { parent.state = isExpanded ?  "default" : "robotArm"}
    }
    ChapterStep {
        id: stepIrrigation
        onClicked: { parent.state = isExpanded ? "default" : "irrigation"}
    }
    ChapterStep {
        id: stepCalibrate
        onClicked: { parent.state = isExpanded ? "default" : "calibrate"}
    }
    ChapterStep {
        id: stepPrepPatient
        onClicked: { parent.state = isExpanded ? "default" : "prepPatient"}
    }
    ChapterStep {
        id: stepRegPatient
        onClicked: { parent.state = isExpanded ? "default" : "regPatient"}
    }
    ChapterStep {
        id: stepAccuracy
        onClicked: { parent.state = isExpanded ? "default" : "accuracy"}
    }


    states: [
        State {
            name: "default"
            PropertyChanges {
                target: stepRobotArm
                state: "state_selected_No_step_robotArm"
            }
            PropertyChanges {
                target: stepIrrigation
                state: "state_selected_No_step_irrigation"
            }
            PropertyChanges {
                target: stepCalibrate
                state: "state_selected_No_step_calibrate"
            }
            PropertyChanges {
                target: stepPrepPatient
                state: "state_selected_No_step_prepPatient"
            }
            PropertyChanges {
                target: stepRegPatient
                state: "state_selected_No_step_regPatient"
            }
            PropertyChanges {
                target: stepAccuracy
                state: "state_selected_No_step_accuracy"
            }
        },
        State {
            name: "robotArm"
            extend: "default"
            PropertyChanges {
                target: stepRobotArm
                state: "state_selected_Yes_step_robotArm"
            }
        },
        State {
            name: "irrigation"
            extend: "default"
            PropertyChanges {
                target: stepIrrigation
                state: "state_selected_Yes_step_irrigation"
            }
        },
        State {
            name: "calibrate"
            extend: "default"
            PropertyChanges {
                target: stepCalibrate
                state: "state_selected_Yes_step_calibrate"
            }
        },
        State {
            name: "prepPatient"
            extend: "default"
            PropertyChanges {
                target: stepPrepPatient
                state: "state_selected_Yes_step_prepPatient"
            }
        },
        State {
            name: "regPatient"
            extend: "default"
            PropertyChanges {
                target: stepRegPatient
                state: "state_selected_Yes_step_regPatient"
            }
        },
        State {
            name: "accuracy"
            extend: "default"
            PropertyChanges {
                target: stepAccuracy
                state: "state_selected_Yes_step_accuracy"
            }
        }
    ]
}
