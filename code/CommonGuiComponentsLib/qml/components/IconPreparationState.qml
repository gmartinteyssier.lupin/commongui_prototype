import QtQuick

import "../figma_raw"

Item {
    width: 32
    height: 32
    state: "crown"

    PreparationIcon {
        id: preparationIcon
        anchors.fill:parent
    }
    states : [
        State {
            name: "crown"

            PropertyChanges {
                target: preparationIcon
                state: "state_property_1_ic_crown"
            }
        },
        State {
            name: "veneer"

            PropertyChanges {
                target: preparationIcon
                state: "state_property_1_ic_facette"
            }
        },
        State {
            name: "implant"

            PropertyChanges {
                target: preparationIcon
                state: "state_property_1_ic_implant"
            }
        }
    ]
}
