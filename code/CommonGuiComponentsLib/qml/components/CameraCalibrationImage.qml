import QtQuick

import "../figma_raw"

Item {

    ImageCalibrate {
        id: imageCalibrate
        transitions: Transition {
            from: "*"; to: "*";
            SequentialAnimation {
                NumberAnimation {
                    properties: "visible";
                    duration: 0
                }
                NumberAnimation {
                    properties: "x,y,width,height,anchors.verticalCenterOffset,anchors.horizontalCenterOffset";
                    easing.type: Easing.InOutQuad
                    duration: 1000
                }
            }
        }
    }

    states: [
        State {
            name: "robotStabilization"

            PropertyChanges {
                target: imageCalibrate
                state: "state_property_1_robotStabilization"
            }
        },
        State {
            name: "patientTrackerCamera"

            PropertyChanges {
                target: imageCalibrate
                state: "state_property_1_patientTrackerCamera"
            }
        },
        State {
            name: "pointerCalibrationVerso"

            PropertyChanges {
                target: imageCalibrate
                state: "state_property_1_pointerCalibrationVerso"
            }
        },
        State {
            name: "pointerCalibrationRecto"

            PropertyChanges {
                target: imageCalibrate
                state: "state_property_1_pointerCalibrationRecto"
            }
        },
        State {
            name: "robotTrackerCalibration"

            PropertyChanges {
                target: imageCalibrate
                state: "state_property_1_robotTrackerCalibration"
            }
        }
    ]
}
