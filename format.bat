@echo off

where clang-format-14.exe >nul 2>nul || (echo clang-format not found! && GOTO:FAILURE)

set SRC=%~dp0code

for /r "%SRC%" %%f in ("*.cpp" "*.h") do (
	echo - %%f
	clang-format-14.exe -i %%f || GOTO:FAILURE
)

GOTO:EOF

:FAILURE
echo Failure!
pause
