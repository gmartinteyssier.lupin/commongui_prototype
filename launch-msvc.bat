@echo off
REM script to be used on Windows to generate and open a VC++ solution

REM make Qt visible
if not exist "%QTDIR%\bin" (echo invalid Qt directory: "QTDIR=%QTDIR%" && GOTO:FAILURE)
set PATH=%QTDIR%\bin;%PATH%

REM find VC++
set "VCPATH=C:\Program Files\Microsoft Visual Studio\2022\Professional"
if not exist "%VCPATH%" (
    set "VCPATH=C:\Program Files\Microsoft Visual Studio\2022\Community"
)

REM make VC++ with cmake & ninja visible in the PATH
REM this is important so that Conan can find those tools if it needs to build some dependencies
call "%VCPATH%\VC\Auxiliary\Build\vcvars64.bat" || GOTO:FAILURE

REM where cmake will put the build files (as specified in CMakePresets.json)
set BUILDDIR=%~dp0build\visual-studio

REM generate build files (if not already done)
REM since we can't know in advance the name of the SLN file cmake will create,
REM we rely on the file RUN_TESTS.vcxproj that is created if cmake configuration succeeded
if not exist "%BUILDDIR%\RUN_TESTS.vcxproj" (
    REM remove any existing build directory (likely from a failed configuration)
    REM and create a new folder with NTFS compression enabled
    if exist "%BUILDDIR%" rmdir /s/q %BUILDDIR%
    mkdir %BUILDDIR% || GOTO:FAILURE
    compact /c /q "%BUILDDIR%" > nul
    REM now run cmake with the appropriate preset
    cmake --preset="visual-studio" || GOTO:FAILURE
)

REM find the VC++ solution file to open
echo.
for /r "%BUILDDIR%" %%f in ("*.sln") do (
    start "Opening VC++ solution" "%%f"
    GOTO:EOF
)
echo Failed to find a VC++ solution to open in "%BUILDDIR%"

:FAILURE
echo Failure!
pause